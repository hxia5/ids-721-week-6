use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use tracing::{info};
use anyhow::anyhow;

// Request structure expects a command ("uppercase" or "lowercase") and a message to convert.
#[derive(Deserialize)]
struct Request {
    command: String,
    message: String,
}

// Response structure to send back a transformed message.
#[derive(Serialize, Debug)]
struct Response {
    transformed_message: String,
} 

// The main handler for the Lambda function.
async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let (event, _context) = event.into_parts();
    let transformed_message = match event.command.as_str() {
        "encrypt" => encrypt(&event.message),
        "decrypt" => decrypt(&event.message),
        _ => return Err(anyhow!("Invalid command").into()),
    };

    // Log information about the response
    let response = Response { transformed_message };
    info!("Response: {:?}", response);

    Ok(response)
}

fn encrypt(text: &str) -> String {
    let mut result = String::new();
    for c in text.chars() {
        if c.is_ascii_alphabetic() {
            let base = if c.is_ascii_lowercase() { b'a' } else { b'A' };
            let offset = (c as u8 - base + 10) % 26;
            result.push((base + offset) as char);
        } else {
            result.push(c);
        }
    }
    result
}

fn decrypt(text: &str) -> String {
    let mut result = String::new();
    for c in text.chars() {
        if c.is_ascii_alphabetic() {
            let base = if c.is_ascii_lowercase() { b'a' } else { b'A' };
            let offset = (c as u8 - base + 16) % 26;
            result.push((base + offset) as char);
        } else {
            result.push(c);
        }
    }
    result
}


#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}


