# IDS 721 Week 2
[![pipeline status](https://gitlab.com/hxia5/ids-721-week-6/badges/main/pipeline.svg)](https://gitlab.com/hxia5/ids-721-week-6/-/commits/main)

## AWS API Endpoint
https://9fjvy6lfp7.execute-api.us-east-1.amazonaws.com/test1/ids-721-week-6

## Overview
* This is my repository ofIDS 721 Mini-Project 6 - Instrument a Rust Lambda Function with Logging and Tracing.

## Purpose
- Add logging to a Rust Lambda function
- Integrate AWS X-Ray tracing
- Connect logs/traces to CloudWatch

## Key Steps

1. Install Rust and Cargo-Lambda, instructions can be found [here](https://www.cargo-lambda.info/guide/installation.html) and [here](https://www.rust-lang.org/tools/install).

2. Create a new Rust project using the following command:
```bash
cargo new ids-721-week-6
```

3. Add `encrypt` and `decrypt` function to `main.rs` and modify `function_handler` to call `encrypt` and `decrypt` functions. Also, add logging and tracing to the function like below:
```rust
// Log information about the response
    let response = Response { transformed_message };
    info!("Response: {:?}", response);

    Ok(response)
```

3. Based on my code, ddd the following dependencies to the Cargo.toml file:
```bash
[dependencies]
anyhow = "1.0"
lambda_runtime = "0.8.3"
serde = "1.0.136"
tokio = { version = "1", features = ["macros"] }
tracing = { version = "0.1", features = ["log"] }
tracing-subscriber = { version = "0.3", default-features = false, features = ["env-filter", "fmt"] }
```

5. Run `cargo lambda watch` in one terminal, and `cargo lambda invoke --data-ascii '{ "command": "encrypt", "message": "Your message" }'` in another terminal to test the function.

6. Sign up for an AWS account and create a new IAM user with programmatic access and attach the `lambdafullaccess` and `iamfullaccess` policy.

7. Click on the user, under the `Security credentials` tab, click on `Create access key`.

8. Copy the `access key ID` and `secret access key`, select a region under the tab at the top right corner. Create a `.env` file in the root directory of the project and add the `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_REGION`.

9. Put `.env` in `.gitignore` to avoid exposing sensitive information.

10. Build the project using `cargo lambda build` and deploy the project using `cargo lambda deploy`. However, there's something wrong with my laptop, since my code can run, I use `Makefile` to deploy the project.

11. Create a `.yml` file for the pipeline and enable auto build, test, and deploy of your lambda function every time you push.

12. After it paased, search for `Lambda` in the AWS Management Console, after redirecting to the Lambda service, make sure the region is the same as the one in the `.env` file.

13. Your function should be deployed and ready to be tested. Click on the function and click on `Add trigger` to test the function.

14. Select `API Gateway`, `Create a new API`, and `REST API`. Set the Security as `Open`, click on `Additional settings` and set the stage name as you want and then click on `Add`.

15. Click on the API you just created, in `Resource`, under `Integration request`, make sure `Lambda proxy integration` is set as `False` and select the recommended settings for `Input passthrough`.

16. Go to IAM, click on `Roles`, and click on `create role`. Select `AWS service`, `API Gateway`. Copy ARN, then go back to the API web page, go to setting and put the ARN under `Logging`. Then, go to `Stage` and click on `Logs/Tracing`, and enable `Log full requests/responses data` and `Enable X-Ray Tracing`.

![alt text](api.png)

17. Click on `Deploy API` and select the stage you just created, and click on `Deploy`.

18. After getting the endpoint, use the command 
```
curl -X POST <your endpoint> \
  -H 'content-type: application/json' \
  -d '{ "command": "encrypt", "message": "This is Harry" }'
```
to test the function. 

19. Go to CloudWatch, click on `Log groups`, and click on the log group you just created. You should be able to see the logs.

20. Go to X-Ray, click on `Traces`, and you should be able to see the traces.

## AWS X-ray Tracing

![alt text](x-ray.png)

## Cloudwatch Centeralized Logging

![alt text](cloud.png)

## References

https://docs.aws.amazon.com/lambda/latest/dg/services-xray.html